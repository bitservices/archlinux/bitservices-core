# Maintainer: Richard Lees <git0@bitservices.io>
###############################################################################

pkgname=bitservices-core
pkgver=0.0.4
pkgrel=1
pkgdesc="BITServices Core Archlinux Configuration"
arch=('any')
install="${pkgname}.install"
license=('none')
depends=('pacman'
         'reflector'
         'rsync')
source=('makepkg-no-compress.conf'
        'makepkg-no-debug.conf'
        'modprobe-pcspkr.conf'
        'modprobe-speck.conf'
        'pacman-hook-reflector.hook'
        'profile-colours.sh'
        'profile-go.sh'
        'profile-helm.sh'
        'profile-terraform.sh'
        'profile-tools.sh'
        'sysctl-swap.conf'
        'sysctl-sysrq.conf'
        'sysctl-torrent.conf'
        'systemd-swap.service'
        'udev-android.rules'
        'udev-schedulers.rules')
sha256sums=('4680ce462590c5d1abd7012bdb09dcfff64d4fe5486dc0104c1971e233b79918'
            '9d56f2017317d5efab7065d9823d89ebd23fe1f5d3986740a8b084c73c428be4'
            '45fcf00e3f7177c42f18c7b5bf2ed2074c11d440404705e082a5f9eea52d30b8'
            '007a7fafe3353bdd9e80ba61224522b419a8a155cabc348d8a5ffb2cb6f66379'
            'f60abe1dacfd01f2bf66a85e045812c4aaac0d15167e280411b3a9daa0924934'
            'c2b1cc07440ec1f6e399fad987d734ce7c0b976c21206f3b2ae8c2c7d69fd746'
            '38ab6717e859a7a8217c90a99cabdb8cface3b82e3347e86c6d9de38585b804d'
            'ba81550f2de5257c2ec11dcca6dbe9de295b6a944882b00b7fc7ef630a231e88'
            '0fc6e85406a648ba32d9616226d929cf267c3c8b83c7114a7036e21740e438a2'
            'da5864dd51d2063da4684b55960505ecce8951954f361269d42ca563aae784ec'
            'dc675677ebcc291db589f8a697aee9725644fe9fdae2bcad315cb8b2e958460c'
            'd0988afd467bcc82c36d0ce2e116302625a139f5409b0331c602712f23b3b77f'
            '7bf24b153e4a0813ccf15a68e5b63b5fe2b18e8a16c6dfad13fc1f8ee3c90067'
            'fa78b08540ee989058c559b4efe40a06a1cdfc98c5b86c66c697e42bec242272'
            'bc103f6c20fafa44bf38bb30c3b240d94a67a05ac62244d107b36f940de4ced4'
            '876bedf3b8aaa2e7969d5e9b2fc0e07e8f881c1138941d2e4b7d285715499b8a')

###############################################################################

package() {
  install -d "${pkgdir}/etc/makepkg.conf.d"
  install -d "${pkgdir}/etc/profile.d"
  install -d "${pkgdir}/usr/lib"
  install -d "${pkgdir}/usr/lib/modprobe.d"
  install -d "${pkgdir}/usr/lib/sysctl.d"
  install -d "${pkgdir}/usr/lib/systemd"
  install -d "${pkgdir}/usr/lib/systemd/system"
  install -d "${pkgdir}/usr/lib/udev"
  install -d "${pkgdir}/usr/lib/udev/rules.d"
  install -d "${pkgdir}/usr/share"
  install -d "${pkgdir}/usr/share/libalpm"
  install -d "${pkgdir}/usr/share/libalpm/hooks"

  install -m644 "${srcdir}/makepkg-no-compress.conf" "${pkgdir}/etc/makepkg.conf.d/${pkgname}-no-compress.conf"
  install -m644 "${srcdir}/makepkg-no-debug.conf" "${pkgdir}/etc/makepkg.conf.d/${pkgname}-no-debug.conf"

  install -m644 "${srcdir}/modprobe-pcspkr.conf" "${pkgdir}/usr/lib/modprobe.d/${pkgname}-pcspkr.conf"
  install -m644 "${srcdir}/modprobe-speck.conf" "${pkgdir}/usr/lib/modprobe.d/${pkgname}-speck.conf"

  install -m644 "${srcdir}/pacman-hook-reflector.hook" "${pkgdir}/usr/share/libalpm/hooks/${pkgname}-reflector.hook"

  install -m644 "${srcdir}/profile-colours.sh" "${pkgdir}/etc/profile.d/${pkgname}-colours.sh"
  install -m644 "${srcdir}/profile-go.sh" "${pkgdir}/etc/profile.d/${pkgname}-go.sh"
  install -m644 "${srcdir}/profile-helm.sh" "${pkgdir}/etc/profile.d/${pkgname}-helm.sh"
  install -m644 "${srcdir}/profile-terraform.sh" "${pkgdir}/etc/profile.d/${pkgname}-terraform.sh"
  install -m644 "${srcdir}/profile-tools.sh" "${pkgdir}/etc/profile.d/${pkgname}-tools.sh"

  install -m644 "${srcdir}/sysctl-swap.conf" "${pkgdir}/usr/lib/sysctl.d/99-${pkgname}-swap.conf"
  install -m644 "${srcdir}/sysctl-sysrq.conf" "${pkgdir}/usr/lib/sysctl.d/99-${pkgname}-sysrq.conf"
  install -m644 "${srcdir}/sysctl-torrent.conf" "${pkgdir}/usr/lib/sysctl.d/99-${pkgname}-torrent.conf"

  install -m644 "${srcdir}/systemd-swap.service" "${pkgdir}/usr/lib/systemd/system/${pkgname}-swap.service"

  install -m644 "${srcdir}/udev-android.rules" "${pkgdir}/usr/lib/udev/rules.d/99-${pkgname}-android.rules"
  install -m644 "${srcdir}/udev-schedulers.rules" "${pkgdir}/usr/lib/udev/rules.d/99-${pkgname}-schedulers.rules"
}

###############################################################################
