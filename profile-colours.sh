###############################################################################

export COLOUR_GREY="\033[30m"
export COLOUR_RED="\033[31m"
export COLOUR_GREEN="\033[32m"
export COLOUR_YELLOW="\033[33m"
export COLOUR_BLUE="\033[34m"
export COLOUR_PURPLE="\033[35m"
export COLOUR_CYAN="\033[36m"
export COLOUR_WHITE="\033[37m"
export COLOUR_BOLD="\033[1m"
export COLOUR_RESET="\033[m"

###############################################################################
