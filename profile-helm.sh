###############################################################################

HELM_ROOT="${HOME}/.helm.d"
HELM_BINARIES="${HELM_ROOT}/binary-cache"

if [ ! -d "${HELM_ROOT}"     ]; then mkdir "${HELM_ROOT}"    ; chmod 700 "${HELM_ROOT}"    ; fi
if [ ! -d "${HELM_BINARIES}" ]; then mkdir "${HELM_BINARIES}"; chmod 700 "${HELM_BINARIES}"; fi

if [ -d "${HELM_BINARIES}" ]; then
  export HELM_BINARY_CACHE="${HELM_BINARIES}"
fi

###############################################################################
