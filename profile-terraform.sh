###############################################################################

TERRAFORM_ROOT="${HOME}/.terraform.d"
TERRAFORM_BINARIES="${TERRAFORM_ROOT}/binary-cache"
TERRAFORM_PLUGINS="${TERRAFORM_ROOT}/plugin-cache"

if [ ! -d "${TERRAFORM_ROOT}"     ]; then mkdir "${TERRAFORM_ROOT}"    ; chmod 700 "${TERRAFORM_ROOT}"    ; fi
if [ ! -d "${TERRAFORM_BINARIES}" ]; then mkdir "${TERRAFORM_BINARIES}"; chmod 700 "${TERRAFORM_BINARIES}"; fi
if [ ! -d "${TERRAFORM_PLUGINS}"  ]; then mkdir "${TERRAFORM_PLUGINS}" ; chmod 700 "${TERRAFORM_PLUGINS}" ; fi

if [ -d "${TERRAFORM_BINARIES}" ]; then
  export TERRAFORM_BINARY_CACHE="${TERRAFORM_BINARIES}"
fi

if [ -d "${TERRAFORM_PLUGINS}" ]; then
  export TERRAFORM_PLUGIN_CACHE="${TERRAFORM_PLUGINS}"
fi

###############################################################################
